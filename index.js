//imports des bibliothèques, middlewares et controllers
const express = require("express");
require("dotenv").config();
const connection = require("./config/db");
const cors = require("cors");
const app = express();
// importation de bcrypt
const bcrypt = require('bcrypt');


// Middlewares
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors({ origin: "http://localhost:8080" }));


//route de création de compte
app.post("/register", (req, res) => {
  const { email, password } = req.body;
  if (!email || !password) {
    res.status(400).json({ error: "Please specify both email and password" });
  } else {
    // hacher le mot de passe (ici 10 correspond au nombre de "salt round")
    bcrypt.hash(password, 10, function (bcryptError, hashedPassword) {
      if (bcryptError) {
        res.status(500).json({ error: bcryptError });
      } else {
        // enregistrer le compte en base de données
        connection.query(
          `INSERT INTO user(email, password) VALUES (?, ?)`,
          [email, hashedPassword],
          (mysqlError, result) => {
            if (mysqlError) {
              res.status(500).json({ error: mysqlError });
            } else {
              // on retourne le compte créé (mais sans le mot de passe !)
              res.status(201).json({
                id: result.insertId,
                email,
              });
            }
          }
        );
      }
    });
  }
});

//route de connexion au compte
app.post("/login", (req, res) => {
  const { email, password } = req.body;
  if (!email || !password) {
    res
      .status(400)
      .json({ errorMessage: "Please specify both email and password" });
  } else {
    // Vérifier qu'un compte existe bien avec l'email fourni.
    connection.query(
      `SELECT * FROM user WHERE email=?`,
      [email],
      (mysqlError, result) => {
        if (mysqlError) {
          res.status(500).json({ error: mysqlError });
        } else if (result.length === 0) {
          res.status(401).json({ error: "Invalid email" });
        } else {
          const user = result[0];
          // Récupérer le mot de passe haché en base de données.
          const hashedPassword = user.password;
          // Comparer avec la méthode bcrypt.compare() le mot de passe haché et le mot de passe fourni en clair.
          bcrypt.compare(
            password,
            hashedPassword,
            function (bcryptError, passwordMatch) {
              if (bcryptError) {
                res.status(500).json({ error: bcryptError });
              } else if (passwordMatch) {
                // passwordMatch est vrai si le mot de passe correspond.
                // Retourner le compte connecté (mais sans le mot de passe !).
                res.status(200).json({
                  id: user.id,
                  email: user.email,
                });
              } else {
                res.status(401).json({ error: "Invalid password" });
              }
            }
          );
        }
      }
    );
  }
});



const port = 3000;
app.listen(port, () => {
  console.log(`Serveur en écoute sur le port ${port}`);
});
